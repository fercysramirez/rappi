<?php

  class Matrix
  {
      public $newArray = array(array(array()));
      public $size, $accumulator;
      public function __construct($size)
    {       
          $this->size = $size;
          for($X = 0; $X < $this->size; $X++){
              //inicializar y
              for($Y = 0; $Y < $this->size; $Y++ ){
                  //inicializar z
                  for($Z = 0; $Z < $this->size; $Z++ ){
                      $this->newArray[$X][$Y][$Z] = 0;           
                  }
              }
          }         
      }
      public function update($x,$y,$z,$value)
    {
            $this->newArray[$x][$y][$z] = $value;
      }
      public function query($x1,$y1,$z1,$x2,$y2,$z2)
      {
           $accumulator=0;
              for($Xa = $x1; $Xa <= $x2; $Xa++){
                for($Ya = $y1; $Ya < $this->size; $Ya++ ){
                    for($Za = $z1; $Za < $this->size; $Za++ ){
                      if ($y2 == $Ya && $x2==$Xa) {
                        if($z2==$Za){
                          $accumulator += $this->newArray[$Xa][$Ya][$Za]; 
                          return $accumulator; 
                        }
                      }
                        $accumulator += $this->newArray[$Xa][$Ya][$Za];        
                    }
                }
            }
        }
    }

  class validate
  {
      public $t,$size,$m,$var1,$var2,$x,$y,$z;

      public function __construct()
    {
                    
      }
      public function validate_T_N_M($t,$size,$m)
    {
      $this->t=$t;
      $this->size=$size;
      $this->m=$m;

      if ($this->t >=1 && $this->t <= 50) {
        if ($this->size >=1 && $this->size <= 100) {
          if ($this->m >=1 && $this->m <= 1000) {
            return "true";
          }else {
            return "Valor de M debe estar en el limite 1 <= M <= 1000, por favor Verificar datos!";
          }
        }else {
        return "Valor de N debe estar en el limite 1 <= N <= 100, por favor Verificar datos!";
        }
      }else {
        return "Valor de T debe estar en el limite 1 <= T <= 50, por favor Verificar datos!";
      }                         
      }
      public function validate_X_Y_Z_QUERY($var1,$var2,$size)
    {
      $this->var1=$var1;
      $this->var2=$var2;
      $this->size=$size;
      if ($this->var1 >=1 && $this->var2 >= $this->var1 && $this->size >= $this->var2) { 
            return "true";
      }
      else{
        return "false";
      }                   
      }
      public function validate_X_Y_Z($x,$y,$z,$size)
    {
      $this->x=$x;
      $this->y=$y;
      $this->z=$z;
      $this->size=$size;
      if ($this->x <= $this->size && $this->y <= $this->size && $this->z <= $this->size && $this->x >= 1 && $this->y >= 1 && $this->z >= 1) {         
        return "true";
      }
      else{
        return "false";
      }                   
      }
  }

//Codigo para ajustar los datos de entrada por consola.
 $general=0; 
while ($general == 0) {  
    echo "T: ";
    fscanf(STDIN, "%d\n", $t);
    echo "N: ";
    fscanf(STDIN, "%d\n", $size);
    echo "M: ";
    fscanf(STDIN, "%d\n", $m); 
 $valida= new validate();
  if($valida->validate_T_N_M($t,$size,$m) != "true"){
      echo "".$valida->validate_T_N_M($t,$size,$m)."\n" ;
  }else{
      $general=1;
      $matrix= new Matrix($size);
      for ($i=1; $i <= $t ; $i++) { 
        for ($j=1; $j <= $m ; $j++) { 
          $control=0;
            while ( $control == 0) {
              echo "1: UPDATE 2: QUERY ";
               fscanf(STDIN, "%d\n", $opc);
              //UPDATE
              if ($opc==1) {
                $aux=0;
                while ( $aux == 0) {
                    echo "X,Y,Z ";
                    fscanf(STDIN, "%s\n", $xyz);
                    list($x, $y, $z) = split('[,.-]', $xyz);
                    $valiValue=0;
                    while ($valiValue <= 0) {
                      echo "VALOR: ";
                      fscanf(STDIN, "%d\n", $valor);
                      if ($valor>= "/^[0-9]+$/") {
                        $valiValue=1;
                      }
                    }                    
                    if ($valida->validate_X_Y_Z($x,$y,$z,$size)=="false" ) {
                      echo "Por favor verificar datos de x,y,z, segun la matriz creada los limites no coinciden! la matriz es ".$size."x".$size."x".$size." \n";
                    }else{
                      $aux=1;
                      $matrix->update($x-1,$y-1,$z-1,$valor);
                    }
                }
                $opc=0;$control=1;
              }
              if ($opc ==2) {
                  $aux=0;
                while ( $aux == 0) {
                    echo "X1,Y1,Z1 ";
                    fscanf(STDIN, "%s\n", $xyz1);
                    list($x1, $y1, $z1) = split('[,.-]', $xyz1);
                    echo "X2,Y2,Z2 ";
                    fscanf(STDIN, "%s\n", $xyz2);
                    list($x2, $y2, $z2) = split('[,.-]', $xyz2);
                    if ($valida->validate_X_Y_Z_QUERY($x1,$x2,$size)=="false" || $valida->validate_X_Y_Z_QUERY($y1,$y2,$size)=="false" || $valida->validate_X_Y_Z_QUERY($z1,$z2,$size)=="false") {
                      echo "Por favor verificar datos"."\n";
                    }else{
                      $aux=1;
                      echo "Suma: ". $matrix->query($x1-1,$y1-1,$z1-1,$x2-1,$y2-1,$z2-1)."\n";
                    }
                }
                $opc=0;
                $control=1;
              }
            } 
        }
        if ($i!=$t) {
            echo "SEGUNTO TEST \n";
            echo "N: ";
            fscanf(STDIN, "%d\n", $size);
            echo "M: ";
            fscanf(STDIN, "%d\n", $m);
            $matrix= new Matrix($size);
          } 

      }
  }
}
?>
